import { useEffect, useState } from 'react';

function Count () {
    const [count, setCount] = useState(0);
    const increaseCount = () => {
        console.log("increase count ...");
        setCount(count + 1)
    }
    useEffect(() => {
        document.title = `You clicked ${count} times!`
    })
    return (
        <div>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCount}>Click me</button>
        </div>
    )
}

export default Count;
